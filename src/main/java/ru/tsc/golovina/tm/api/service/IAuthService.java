package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getCurrentUserId();

    void setCurrentUserId(@Nullable String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void checkRoles(@Nullable Role... roles);

}
