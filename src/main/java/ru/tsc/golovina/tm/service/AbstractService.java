package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.api.IService;
import ru.tsc.golovina.tm.exception.empty.EmptyIdException;
import ru.tsc.golovina.tm.exception.empty.EmptyIndexException;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;
import ru.tsc.golovina.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.add(entity);
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) {
        if (index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        @Nullable final E entity = repository.findByIndex(index);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public boolean existsByIndex(@Nullable final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        return repository.existsByIndex(index);
    }

    @NotNull
    @Override
    public E removeById(final @NotNull String id) {
        @Nullable final E entity = repository.removeById(id);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable final Integer index) {
        if (index == null) throw new EmptyIndexException();
        if (index > getSize() || index < 0) throw new IndexIncorrectException();
        @Nullable final E entity = repository.removeByIndex(index);
        if (entity == null) throw new EntityNotFoundException();
        return entity;
    }

    @NotNull
    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
