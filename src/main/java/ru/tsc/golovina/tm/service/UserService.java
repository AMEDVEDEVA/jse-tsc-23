package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.entity.UserEmailExistsException;
import ru.tsc.golovina.tm.exception.entity.UserLoginExistsException;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new UserLoginExistsException(login);
        @NotNull final User user = new User(login, HashUtil.salt(password));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findById(id);
        user.setPassword(HashUtil.salt(password));
    }

    @Override
    public void setRole(@Nullable final String id, @Nullable final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = findById(id);
        user.setRole(role);
    }

    @NotNull
    @Override
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findUserByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = userRepository.findUserByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void removeUserById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

    @NotNull
    @Override
    public User removeUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        return userRepository.findUserByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return userRepository.findUserByEmail(email) != null;
    }

    @Override
    public void updateUserById(
            @Nullable final String id, @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName, @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        @NotNull final User user = repository.findById(id);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
    }

    @Override
    public void updateUserByLogin(
            @Nullable final String login, @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName, @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExists(email)) throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findUserByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findUserByLogin(login);
        user.setLocked(false);
    }

}