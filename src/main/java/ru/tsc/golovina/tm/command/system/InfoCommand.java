package ru.tsc.golovina.tm.command.system;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.util.NumberUtil;

public class InfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "info";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display system info";
    }

    @Override
    public void execute() {
        @NotNull final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        @NotNull final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
