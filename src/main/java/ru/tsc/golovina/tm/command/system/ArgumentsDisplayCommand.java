package ru.tsc.golovina.tm.command.system;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "arguments";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        @Nullable final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments)
            System.out.println(argument.getCommand());
    }

}
