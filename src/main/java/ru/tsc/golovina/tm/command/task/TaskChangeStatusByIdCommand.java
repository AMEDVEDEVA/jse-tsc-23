package ru.tsc.golovina.tm.command.task;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-change-status-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change task status by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusValue);
        serviceLocator.getTaskService().changeStatusById(userId, id, status);
    }

}
