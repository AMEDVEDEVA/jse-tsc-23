package ru.tsc.golovina.tm.command.task;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class TaskListByProjectIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-list-by-project-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of tasks by project id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        showProjectTasks(project);
    }

}
