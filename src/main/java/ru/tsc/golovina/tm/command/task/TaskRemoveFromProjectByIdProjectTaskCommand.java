package ru.tsc.golovina.tm.command.task;

import com.sun.istack.internal.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractProjectTaskCommand;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.util.TerminalUtil;

public final class TaskRemoveFromProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getCommand() {
        return "task-remove-from-project-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Removes task from project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
    }

}
