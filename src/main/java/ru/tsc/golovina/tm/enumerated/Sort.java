package ru.tsc.golovina.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.comparator.ComparatorByCreated;
import ru.tsc.golovina.tm.comparator.ComparatorByName;
import ru.tsc.golovina.tm.comparator.ComparatorByStartDate;
import ru.tsc.golovina.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by sreated", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", ComparatorByStartDate.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(@NotNull String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}