package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }
}
