package ru.tsc.golovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.entity.IWBS;
import ru.tsc.golovina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractOwnerEntity implements IWBS {

    public Project() {
    }

    public Project(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull private String name = "";

    @NotNull private String description = "";

    @NotNull private Status status = Status.NOT_STARTED;

    @Nullable private Date startDate;

    @Nullable private Date finishDate;

    @NotNull private Date created = new Date();

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull final String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @NotNull
    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

    @Nullable
    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(@Nullable final Date startDate) {
        this.startDate = startDate;
    }
}