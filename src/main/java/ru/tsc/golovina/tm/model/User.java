package ru.tsc.golovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.util.HashUtil;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull private String login;

    @NotNull private String password;

    @Nullable private String email;

    @Nullable private String lastName;

    @Nullable private String firstName;

    @Nullable private String middleName;

    @NotNull private Role role = Role.USER;

    private Boolean locked = false;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public User() {
    }

    public User(@NotNull final String login, @NotNull final String password) {
        this.login = login;
        this.password = HashUtil.salt(password);
    }

    public void setLogin(@NotNull final String login) {
        this.login = login;
    }

    public void setPassword(@NotNull final String password) {
        this.password = password;
    }

    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

    public void setLastName(@Nullable final String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(@Nullable final String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(@Nullable final String middleName) {
        this.middleName = middleName;
    }

    public void setRole(@NotNull final Role role) {
        this.role = role;
    }

    @NotNull public String getLogin() {
        return login;
    }

    @NotNull public String getPassword() {
        return password;
    }

    @Nullable public String getEmail() {
        return email;
    }

    @Nullable public String getLastName() {
        return lastName;
    }

    @Nullable public String getFirstName() {
        return firstName;
    }

    @Nullable public String getMiddleName() {
        return middleName;
    }

    @NotNull public Role getRole() {
        return role;
    }

}