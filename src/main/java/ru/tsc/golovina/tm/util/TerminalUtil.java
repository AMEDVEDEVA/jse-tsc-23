package ru.tsc.golovina.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull RuntimeException e) {
            throw new IndexIncorrectException(value);
        }
    }

}
