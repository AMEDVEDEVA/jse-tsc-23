package ru.tsc.golovina.tm.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.repository.IAuthRepository;

@Getter
@Setter
public final class AuthRepository implements IAuthRepository {

    @NotNull private String currentUserId;

    @Override
    public @NotNull String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(@NotNull final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
