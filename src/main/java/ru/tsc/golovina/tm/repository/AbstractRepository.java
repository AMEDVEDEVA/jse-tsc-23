package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.exception.entity.EntityNotFoundException;
import ru.tsc.golovina.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        list.sort(comparator);
        return list;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        return index < list.size() && index >= 0;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElseThrow(EntityNotFoundException::new);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final Integer index) {
        return list.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @Nullable final E entity = findById(id);
        list.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @NotNull
    @Override
    public Integer getSize() {
        return list.size();
    }

}
