# TASK MANAGER

## DEVELOPER INFO

**name:** Alla Golovina

**email:** amedvedeva@tsconsulting.com

**email:** eclipese@ya.ru

## HARDWARE

**CPU:** i5

**RAM:** 16GB

**SSD:** 512GB

## SOFTWARE

**System:** Windows 10

**Version JDK:** 1.8.0_282

## APPLICATION BUILD 

```bash
mvn clean install
```

## APPLICATION RUN

```bash
java -jar task-manager.jar
```

## TASK SCREENSHOTS

https://disk.yandex.ru/d/_XrtjAkX1S2aeg?w=1